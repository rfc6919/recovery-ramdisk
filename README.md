# Ten64 Recovery Firmware

The Ten64 recovery system is a built-in ramdisk on the SPI-NAND
containing useful tools for writing operating system images to SSD's, 
chroot'ing into a existing install or to update low level firmware.

As the name might suggest, it's purpose is similar to the recovery feature on Android phones.

The recovery system is based on OpenWrt, you can customize it for your own needs, 
such as performing automated image download.

You can also run the recovery image from other media sources such as USB or TFTP.

For more information, please see the [recovery page](https://ten64doc.traverse.com.au/software/recovery/)
in the Ten64 documentation

## Building
The Recovery firmware uses artefacts (packages and ImageBuilder) from [μVirt](https://gitlab.com/traversetech/muvirt) 
to build, as certain components (such as the bare-metal appliance store)
share common components with the μVirt ones.

The imagebuilder is sourced from the url in `imagebuilder-url.txt`. 

This also means we can avoid doing a full OpenWrt system build for most changes.

To build, run `./build.sh`. 

## Testing
You can test the image without having to overwrite your existing recovery firmware
by using a USB drive or TFTP.

### On real hardware
TFTP:
```
tftp $load_addr recovery.itb && mtd read dpl 0x80100000 && fsl_mc apply DPL 0x80100000 && bootm $load_addr#ten64
```

USB:
```
usb start
fatload usb 0 $load_addr recovery.itb && mtd read dpl 0x80100000 && fsl_mc apply DPL 0x80100000 && bootm $load_addr#ten64
```

### Inside a VM
The `test/launch_test_vm.sh` script launches a QEMU instance. For best results, run
on a native ARM64 host with KVM capability.

As the test VM uses the emulated QEMU device tree, please be aware that the system will
identify itself as such, this can affect things such as the OpenWrt system defaults
and the compatible images presented by the appliance store.

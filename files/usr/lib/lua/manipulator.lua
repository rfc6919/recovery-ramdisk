local plutils = require "pl.utils"
local plfile = require "pl.file"
local pldir = require "pl.dir"
local plstringio = require "pl.stringio"
local plstringx = require "pl.stringx"

local lyaml = require "lyaml"
local cconfig = require "cconfig"

local manipulator = {}

function manipulator.expand_gpt(device)
    local expand_command = "printf \"fix\n\" | parted ---pretend-input-tty "..device.." print"
    local expand_success, expand_return, expand_stdout, expand_stderr = plutils.executeex(expand_command, false)
    return expand_success
end

function manipulator.run_parted(device, command)
    local parted_command = string.format("parted -s -a optimal -- %s %s", device, plutils.quote_arg(command))
    local parted_success, parted_return, parted_stdout, parted_stderr  = plutils.executeex(parted_command, false)
    if (parted_success ~= true) then
        print("WARNING: parted command " .. " did not succeed")
        print("Output from parted is: " .. parted_stderr)
    end
    return parted_success
end

function manipulator.get_target_info(mountpoint)
    local os_release_filepath = mountpoint .. "/etc/os-release"
    local os_release = plfile.read(os_release_filepath)

    if (os_release == nil) then
        error("Could not read " .. os_release_filepath)
    end

    local os_release_data = {}
    local lines = plstringx.splitlines(os_release)

    for k,v in pairs(lines) do
        local key,value = nil,nil
        if (v:sub(#v) == "\"") then
                key,value = v:match("([%w|_]+)=(%b\"\")")
                os_release_data[key] = value:sub(2,value:len()-1)
        else
                key,value = v:match("([%w|_]+)=([%w]+)")
                os_release_data[key] = value
        end
    end

    return os_release_data
end

function manipulator.do_mount(partdevice, mountpoint, is_bind)
    local is_bind_arg = ""
    if (is_bind) then
        is_bind_arg = "--bind "
    end
    local mount_command = "mount "..is_bind_arg .. partdevice .. " " .. mountpoint
    local mount_success, mount_return, mount_stdout, mount_stderr = plutils.executeex(mount_command, false)
    if (mount_success == false) then
        print("ERROR: Failed to mount " .. partdevice .. " at " .. mountpoint)
        print(mount_stderr)
        return false
    end
    return true
end

function manipulator.create_swap_partition(partdevice)
    local mkswap_success, mkswap_return, mkswap_stdout, mkswap_stderr = plutils.executeex("mkswap "..partdevice)
    return mkswap_success
end

function manipulator.get_partition_dev_for_device(device, part_idx)
    lastchar = device:sub(-1)
    if (tonumber(lastchar) == nil) then
        return device .. part_idx
    end
    return device .. "p" .. part_idx
end

function manipulator.resize_ext4(rootpart_device)
    local e2fsck_success,e2f_ret,e2f_out,e2f_err = plutils.executeex("e2fsck -p -f " .. rootpart_device)
    if (e2fsck_success == false) then
        error("File system check on " .. rootpart_device .. "failed: " .. e2f_err)
        return false
    end

    local resize2fs_success, resize2fs_ret, 
        resize2fs_stdout, resize2fs_stderr = plutils.executeex("resize2fs " .. rootpart_device)
    if (resize2fs_success == false) then
        error("Failed to resize root partition: " .. resize2fs_stderr)
    end
end

function manipulator.resize_xfs(partdevice, online_mountpoint)
    -- xfs is different to ext4 as the actual resize has to happen 'online'
    manipulator.do_mount(partdevice, online_mountpoint, false)

    local growxfs_success, growxfs_ret, growxfs_stdout, growxfs_stderr =
        plutils.executeex("xfs_growfs " .. partdevice)

    if (growxfs_success == false) then
        error("Failed to resize XFS partition ("..partdevice.."): " .. growxfs_stderr)
    end

    local umount_after_resize_success = plutils.executeex("umount " .. online_mountpoint)
    if (umount_after_resize_success == false) then
        error("Failed to unmount partition after live resize")
    end
end

function manipulator.resize_rootfs_and_add_swap(device, rootpart_idx, online_mountpoint, swapsize)
    manipulator.expand_gpt(device)

    local has_resized_part = manipulator.run_parted(device, "resizepart " .. rootpart_idx .. " -" .. swapsize .. "GiB")
    if (has_resized_part ~= true) then error("Could not resize root partition") end

    local has_created_swap = manipulator.run_parted(device, "mkpart swap -"..swapsize.."GiB 100%")
    if (has_created_swap ~= true) then error("Could not create swap partition") end

    plutils.executeex("partprobe " .. device)

    new_swap_part_name = manipulator.get_partition_dev_for_device(device, rootpart_idx+1)

    local create_swap_part = manipulator.create_swap_partition(new_swap_part_name)

    local rootpart_device = manipulator.get_partition_dev_for_device(device, rootpart_idx)

    local rootpart_type = manipulator._call_blkid(rootpart_device, "TYPE")
    print ("Root partition type is " .. rootpart_type)

    if (rootpart_type == "ext4") then
        manipulator.resize_ext4(rootpart_device)
    elseif (rootpart_type == "xfs") then
        manipulator.resize_xfs(rootpart_device, online_mountpoint)
    else
        error("Unsupported filesystem for resize: " .. rootpart_type)
    end
end

function manipulator.format_partition(device, idx, fstype, label)
    local device_ref = manipulator.get_partition_dev_for_device(device, idx)
    local command = nil
    if (fstype == "ext4") then
        command = "mkfs.ext4 "
        if (label ~= nil) then
            command = command .. "-L \""..label.."\" "
        end
        command = command .. device_ref
    elseif (fstype == "fat32") then
        command = "mkfs.fat -F 32 "
        if (label ~= nil) then
            command = command .. "-n \""..label .. "\" "
        end
        command = command .. device_ref
    elseif (fstype == "linux-swap") then
        command = "mkswap " .. device_ref
    else
        error("No format command for type: " .. fstype)
    end
    local format_success, format_ret, format_stdout, format_stderr =
        plutils.executeex(command)
    if (format_success ~= true) then
        error("Error formatting " .. device_ref .. " as " .. fstype .. ":" .. format_stderr)
    end
end

function manipulator.setup_target_chroot(device, rootpart_idx, deployroot)
    local rootpart_device = manipulator.get_partition_dev_for_device(device, rootpart_idx)
    
    local has_mount_root = manipulator.do_mount(rootpart_device, deployroot,false)
    if (has_mount_root == false) then
        return false
    end

    local has_mount_dev = manipulator.do_mount("/dev", deployroot .. "/dev", true)
    local has_mount_sys = manipulator.do_mount("/sys", deployroot .. "/sys", true)
    local has_mount_proc = manipulator.do_mount("/proc", deployroot .. "/proc", true)
end

function manipulator._call_blkid(partdev, field)
    local blkid_cmdline = "blkid -s " .. field .. " -o value " .. partdev
    local blkid_success, blkid_return, blkid_stdout, blkid_stderr = plutils.executeex(blkid_cmdline)
    if (blkid_success) then
        local eol = blkid_stdout:find("\n")
        local uuid = blkid_stdout:sub(0,eol-1)
        return uuid
    end
    error("Could not get UUID for device: " .. partdev .. ":" .. blkid_stderr)
end

function manipulator.get_dev_uuid(partdev)
    return manipulator._call_blkid(partdev, "PARTUUID")
end

function manipulator.get_dev_label(partdev)
    return manipulator._call_blkid(partdev, "LABEL")
end

function manipulator.add_fstab_entry(fstab_file, device, mount, fstype)
    local new_fstab_entry = string.format("PARTUUID=\"%s\"\t%s\t%s\tdefaults\t0\t0\n", device, mount, fstype)

    local fstab_file = io.open(fstab_file,"a")
    fstab_file:write(new_fstab_entry)
    fstab_file:close()
end

function manipulator.add_fstab_entry_device(fstab_file, device, mount, fstype)
    local new_fstab_entry = string.format("%s\t%s\t%s\tdefaults\t0\t0\n", device, mount, fstype)

    local fstab_file = io.open(fstab_file,"a")
    fstab_file:write(new_fstab_entry)
    fstab_file:close()
end

function manipulator.add_swap_fstab(deployroot, device, swappart_idx)
    local swapdev = manipulator.get_partition_dev_for_device(device, swappart_idx)

    local swapuuid = manipulator.get_dev_uuid(swapdev)
    local uuidspec = "UUID="..swapuuid
    manipulator.add_fstab_entry(deployroot .. "/etc/fstab", swapuuid, "swap", "swap")
end

function manipulator.unmount_chroot(deployroot)
    plutils.executeex("umount" .. deployroot .. "/dev")
    plutils.executeex("umount" .. deployroot .. "/sys")
    plutils.executeex("umount" .. deployroot .. "/proc")
end

function manipulator.run_command_in_target(deployroot, command)
    local chroot_command = "chroot " .. deployroot .. " /bin/sh -l -c " .. plutils.quote_arg(command)
    print("Executing: " .. chroot_command)
    local command_success, command_return, command_stdout, command_stderr = plutils.executeex(chroot_command)
    if (command_success == false) then
        error("Failed to run command " .. command .. ": " .. command_stderr)
    end

    return command_success, command_return, cmmand_stdout, command_stderr
end

function manipulator.get_existing_cmdline(deployroot, cmdline_variable)
    local grub_cmdline_var = cmdline_variable or "GRUB_CMDLINE_LINUX_DEFAULT"
    local grub_default_file = deployroot .. "/etc/default/grub"

    local grubcfg = plfile.read(deployroot .. "/etc/default/grub")

    local cmdline_default = grubcfg:match(grub_cmdline_var .. "=(%b\"\")")
    if (cmdline_default ~= nil) then
        local cmdline_default_no_quote = cmdline_default:sub(2,cmdline_default:len()-1)
        return cmdline_default_no_quote
    end
    error("Could not read " .. grub_cmdline_var " from " .. grub_default_file)
end

function manipulator.set_grub_serial_mode(deployroot)
    local grub_default_file = deployroot .. "/etc/default/grub"

    local grubcfg = plfile.read(deployroot .. "/etc/default/grub")
    local grub_serial_lines = "GRUB_TERMINAL=\"serial\"\n"
    local grub_serial_lines = grub_serial_lines .. "GRUB_SERIAL_COMMAND=\"serial --speed=115200 --word=8 --parity=no --stop=1 efi0\"\n"
    local new_grubcfg = grub_serial_lines .. "\n" .. grubcfg

    plfile.write(grub_default_file, new_grubcfg)
end

function manipulator.set_grub_cmdline(deployroot, new_cmdline, cmdline_variable)
    local grub_cmdline_var = cmdline_variable or "GRUB_CMDLINE_LINUX_DEFAULT"
    -- Reset the kernel command line
    local grub_default_file_path = deployroot .. "/etc/default/grub"
    local grub_default_file = plfile.read(grub_default_file_path)
    if (grub_default_file == nil) then
        error("Could not read grub default file at " .. grub_default_file_path)
    end

    local cmdline_linux_default_start = grub_default_file:find(grub_cmdline_var.."=\"")
    if (cmdline_linux_default_start == nil) then error("no "..grub_cmdline_var.." in etc/default/grub") end

    local cmdline_linux_default_end = grub_default_file:find("\n", cmdline_linux_default_start)
    
    new_grub_default_file = grub_default_file:sub(0, cmdline_linux_default_start-1)
    new_grub_default_file = new_grub_default_file .. grub_cmdline_var.."=\"" .. new_cmdline .. "\"\n"
    new_grub_default_file = new_grub_default_file .. grub_default_file:sub(cmdline_linux_default_end+1)

    plfile.write(grub_default_file_path, new_grub_default_file)
end

function manipulator.replace_grub_device_ref_with_uuid(grubfile, rootdev)
    local grubcfg = plfile.read(grubfile)
    local start_grub_linux = grubcfg:find("### BEGIN /etc/grub.d/10_linux ###")
    if (start_grub_linux == nil) then
        error("Could not find start of linux section in " .. grubfile)
    end
    local end_grub_linux = grubcfg:find("### END /etc/grub.d/10_linux ###", start_grub_linux)
    local end_grub_linux_newline = grubcfg:find("\n", end_grub_linux)
    local subsection = grubcfg:sub(start_grub_linux,end_grub_linux_newline+1)

    local uuid = manipulator.get_dev_uuid(rootdev)
    local replaced_string = plstringx.replace(subsection, "root="..rootdev, "root=PARTUUID="..uuid)

    local new_grub_cfg = grubcfg:sub(0,start_grub_linux-1)
    new_grub_cfg = new_grub_cfg .. replaced_string
    new_grub_cfg = new_grub_cfg .. grubcfg:sub(end_grub_linux_newline)

    plfile.write(grubfile, new_grub_cfg)
end

function manipulator.update_blscfg_cmdlines(deployroot, old_cmdline, new_cmdline)
    local entrydir = deployroot .. "/boot/loader/entries"
    local entry_files = pldir.getfiles(entrydir)
    for i,entry_conf_file in pairs(entry_files) do
        local conf_file_entry = plfile.read(entry_conf_file)
        local loc_of_cmdline = conf_file_entry:find(old_cmdline, 0, true)
        if (loc_of_cmdline ~= nil) then
            local cmdline_endline = conf_file_entry:find("\n", loc_of_cmdline, true)

            local updated_conf_file = conf_file_entry:sub(0, loc_of_cmdline-1)
            updated_conf_file = updated_conf_file .. new_cmdline .. "\n"
            updated_conf_file = updated_conf_file .. conf_file_entry:sub(cmdline_endline+1)

            plfile.write(entry_conf_file, updated_conf_file)
        end
    end
end

function manipulator.create_cloud_config(pw, hostname)
    local ccDict = {}
    ccDict["userpw"] = pw
    ccDict["cchostname"] = hostname

    local cloudConfig = cconfig.create_cconfig_for_table(ccDict)
    return cloudConfig
end

function manipulator.save_cconfig(saveloc, cloudConfig, netConfig)
    local userdata_file_path = saveloc .. "/user-data"
    local userdata_file = io.open(userdata_file_path,"w")
    cconfig.serialize_cconfig(userdata_file, cloudConfig)
    userdata_file:close()

    if (netConfig ~= nil) then
        local netdata_file_path = saveloc .. "/network-config"
        local netdata_output = lyaml.dump({netConfig})
        plfile.write(netdata_file_path, netdata_output)
    end

    local metadata_file_path = saveloc .. "/meta-data"
    local metadata_file = io.open(metadata_file_path,"w")
    metadata_file:write("\n")
    metadata_file:close()
end

function manipulator.create_nocloud_config(deployroot, configpartlabel)
    local nocloud_config_document = {}

    local nocloud_datasource = {}
    nocloud_datasource["fs_label"] = configpartlabel

    local datasources = {}
    datasources["NoCloud"] = nocloud_datasource

    nocloud_config_document["datasource"] = datasources

    local nocloud_cloud_d_path = deployroot .. "/etc/cloud/cloud.cfg.d/99_fake_cloud.cfg"
    local output = lyaml.dump({nocloud_config_document})
    outputfile = io.open(nocloud_cloud_d_path,"w")
    -- delete YAML leadin and leadout
    local outputlen = output:len()
    local nowrapper = output:sub(5, outputlen - 5)

    -- cloud-config explictly wants a array rather than multiline sequence,
    -- which is why we don't use lyaml to do this line :(
    outputfile:write("datasource_list: [NoCloud, None]\n")
    outputfile:write(nowrapper)
    outputfile:write("\n")
    outputfile:close()
end


function manipulator.create_embedded_nocloud_config(deployroot, user_data, netconfig_data)
    local nocloud_config_document = {}

    local nocloud_datasource = {}

    local cconfig_file = plstringio.create()
    cconfig.serialize_cconfig(cconfig_file, user_data)
    local cconfig_user_data = cconfig_file:value()
    print(cconfig_user_data)
    nocloud_datasource["user-data"] = cconfig_user_data

    if (netconfig_data ~= nil) then
        local netconfig_encoded = lyaml.dump({netconfig_data})
        nocloud_datasource["network-config"] = netconfig_encoded
    end
    -- some sort of 'meta-data' dictionary is needed otherwise
    -- cloud-config ignores the data source.
    local metadata_placeholder = {}
    metadata_placeholder["_placeholder"] = true
    nocloud_datasource["meta-data"] = metadata_placeholder

    local datasources = {}
    datasources["NoCloud"] = nocloud_datasource

    nocloud_config_document["datasource"] = datasources

    local nocloud_cloud_d_path = deployroot .. "/etc/cloud/cloud.cfg.d/99_fake_cloud.cfg"
    local output = lyaml.dump({nocloud_config_document})
    outputfile = io.open(nocloud_cloud_d_path,"w")
    -- delete YAML leadin and leadout
    local outputlen = output:len()
    local nowrapper = output:sub(5, outputlen - 5)

    -- cloud-config explictly wants a array rather than multiline sequence,
    -- which is why we don't use lyaml to do this line :(
    outputfile:write("datasource_list: [NoCloud, None]\n")
    outputfile:write(nowrapper)
    outputfile:write("\n")
    outputfile:close()
end

function manipulator.get_latest_debian_kernel_ref(repourl, dist)
    local package_urls = {}

    local distpkgs = repourl .. "/dists/" .. dist .. "/main/binary-arm64/Packages"
    print("Getting package info from " .. distpkgs)
    local get_success = plutils.executeex("curl " .. distpkgs .. " -L -o /tmp/distpkgs")
    local distfiles = plfile.read("/tmp/distpkgs")
    -- Find linux-image-traverse
    local has_trav_metapackage = distfiles:find("Package: linux-image-traverse",0, true)
    if (has_trav_metapackage == nil) then
        error("Error: could not find linux-image-traverse reference")
    end

    local depends_line = distfiles:find("Depends: ", has_trav_metapackage, true)
    local depends_end = distfiles:find("\n", depends_line)
    local depends_sub = distfiles:sub(distfiles:find(":", depends_line)+1, depends_end)
    local depends_split = plstringx.split(depends_sub,",")
    
    for idx,pkg in pairs(depends_split) do
        trimmed_pkg = plstringx.strip(pkg)
        local package_line = distfiles:find("Package: "..trimmed_pkg,0,true)
        local filename_line_start = distfiles:find("Filename: ", package_line, true)
        local filename_line = distfiles:sub(
            distfiles:find(":", filename_line_start)+1,
            distfiles:find("\n", filename_line_start)
        )
        local filename_key = repourl .. plstringx.strip(filename_line)
        
        local sha256line_start = distfiles:find("SHA256: ", package_line, true)
        local sha256line = distfiles:sub(
            distfiles:find(":", sha256line_start)+1,
            distfiles:find("\n", sha256line_start)
        )

        local filename_val = plstringx.strip(sha256line)
        package_urls[filename_key] = filename_val
    end
    
    return package_urls
end

function manipulator.download_and_extract_deb_package(url, shasum, extract_root)
    local filename = url:sub(plstringx.rfind(url,"/")+1)
    local in_chroot_extract =  "/tmp/"..filename.."_extract/"
    local extractdir = extract_root .. in_chroot_extract
    pldir.makepath(extractdir)

    local dl_success = plutils.executeex("curl " .. plutils.quote_arg(url) .. " -L -o " .. extractdir .. filename)
    if (dl_success ~= true) then
        error("Failed to download " .. url)
    end

    local extract_debpkg = plutils.executeex("ar x --output=" .. extractdir .. " "  .. extractdir .. filename)
    -- blindly assume data.tar will be xz compressed for now

    -- extraction needs to be done inside the target chroot so symlinks like
    -- /lib -> /lib64 are followed
    -- the '-h' (dereference flag) is also important to ensure tar does not
    -- clobber existing symlinks and permissions
    local extract_success, extract_ret, extract_stdout, extract_stderr =
        manipulator.run_command_in_target(extract_root, "tar --exclude='./usr/*' --exclude='./etc/*' -C /  -Jhxf " .. in_chroot_extract .. "data.tar.xz")
    if (extract_success ~= true) then
      error("Failed to extract " .. extractdir .. "data.tar.xz: " .. extract_stderr)
    end
end

function manipulator.label_fat_filesystem(device, part_idx, label)
    local devicepath = manipulator.get_partition_dev_for_device(device, part_idx)
    plutils.executeex("fatlabel " .. devicepath .. " " .. plutils.quote_arg(label))
end

function manipulator.enable_locales(deployroot, localeprefix)
    local supported_locales = plfile.read(deployroot .. "/usr/share/i18n/SUPPORTED")
    local supported_locales_lines = plstringx.lines(supported_locales)
    local locales_to_gen = {}
    for line in supported_locales_lines do
        if (plstringx.startswith(line, localeprefix)) then
            locales_to_gen[#locales_to_gen +1] = line
        end
    end
    local locales_to_gen_string = plstringx.join("\n", locales_to_gen)
    plfile.write(deployroot .. "/etc/locale.gen", locales_to_gen_string)
end

function manipulator.run_locale_gen(deployroot)
    return manipulator.run_command_in_target(deployroot, "locale-gen")
end

return manipulator

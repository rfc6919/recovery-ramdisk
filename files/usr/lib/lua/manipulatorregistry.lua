local plutils = require "pl.utils"
local plfile = require "pl.file"
local pldir = require "pl.dir"
local plstringio = require "pl.stringio"
local plstringx = require "pl.stringx"
local tablex = require "pl.tablex"
local jsonc = require "luci.jsonc"
local uci = require "luci.model.uci".cursor()

local manipulatorregistry = {}

function manipulatorregistry.get_cache_path()
    local cachepath = nil
    uci:foreach("appstore","appstore", function(s)
        name = s[".name"]
        if (s["manipulatorcache"] ~= nil and cachepath == nil) then
          cachepath = s["manipulatorcache"]
        end
      end)
    if (cachepath == nil) then
       cachepath = "/tmp/lib/manipulatorcache/"
    end
    pcall(pldir.makepath, cachepath)
    return cachepath
end

function manipulatorregistry.download_registry(saveto, url)
   local curlcmd = "curl -L -w %{http_code} " .. plutils.quote_arg(url) .. " -o " .. saveto
   local curl_success, dl_return,
    dl_stdout, dl_stderr = plutils.executeex(curlcmd)
   if (curl_success ~= true) then
    error("Could not download from " .. url .. " error: " .. dl_stderr)
   end
   local http_code = tonumber(dl_stdout)
   if (http_code ~= 200) then
    error("http download failed, HTTP code " .. http_code)
   end
   return true
end

function manipulatorregistry.parse_registry(filename)
    local f = io.open(filename)
    if (f == nill) then
        error("Unable to read " .. filename)
    end
    local manipulatorstring = f:read("*a")
    f:close()
    data = jsonc.parse(manipulatorstring)
    return data
end

function manipulatorregistry.download_registry_to_cache(identifier, saveto_path, url)
    local has_downloaded = manipulatorregistry.download_registry(saveto_path, url)
    if (has_downloaded) then
        local registry_contents = manipulatorregistry.parse_registry(saveto_path)
        local baseurl = registry_contents["baseHref"]
        if (baseurl == nil) then
            baseurl = url:sub(0,plstringx.rfind(url,'/'))
        end
        print("Base url: " .. baseurl)
        registry_contents["baseHref"] = baseurl
        local updated_registry_with_url = jsonc.stringify(registry_contents, true)
        local f = io.open(saveto_path,"w")
        f:write(updated_registry_with_url)
        f:close()
    end
end

function manipulatorregistry.download_registries()
    print("Get cache path")
    local got_cache, cache = pcall(manipulatorregistry.get_cache_path)
    if (got_cache ~= true) then
        error("Could not get a valid registry cache path: " .. cache)
    end
    print("Cache path: " .. cache)
    uci:foreach("appstore","manipulatorregistry", function(s)
        name = s[".name"]
        url = s["url"]
        print(name .. "=" .. url)
        if (url ~= nil) then
            local saveto_path = cache .. name
            local has_downloaded_registry, error_str =
                pcall(manipulatorregistry.download_registry_to_cache, name, saveto_path, url)
        end
      end)
    return false
end

function manipulatorregistry.load_manipulators_from_file(filename)
    local manipulatorlookup = {}
    local manipulatorbykey = {}
    local registrydata = manipulatorregistry.parse_registry(filename)
    local registrybase = registrydata["baseHref"]
    if (registrydata.manipulators == nil) then
        error("No \"manipulators\" table in data from " .. filename)
    end
    local manipulators = registrydata["manipulators"]
    for k,v in pairs(manipulators) do
        local manipulatordef = manipulators[k]
        local manipulatorid = manipulatordef["id"]
        local manipulatorurl = registrybase .. manipulatordef["file"]
        manipulatordef["url"] = manipulatorurl
        ---print("ID: " .. manipulatordef["id"])
        matchpatterns = manipulatordef["appliances"]
        for x,pattern in pairs(matchpatterns) do
            manipulatorlookup[pattern] = manipulatorid
        end
        manipulatorbykey[manipulatorid] = manipulatordef
    end

    return manipulatorlookup, manipulatorbykey
end

function manipulatorregistry.load_manipulators()
    local masterlookup = {}
    local masterkey = {}
    local cachepath = manipulatorregistry.get_cache_path()
    for file in lfs.dir(cachepath) do
        if lfs.attributes(file, 'mode') ~='directory' then
            parse_success, thisbylookup, thisbykey = 
                pcall(manipulatorregistry.load_manipulators_from_file,cachepath..file)
            if (parse_success == true) then
                masterlookup = tablex.merge(masterlookup, thisbylookup, true)
                masterkey = tablex.merge(masterkey, thisbykey, true)
            else
                if (manipulatorregistry._debug) then
                    print("Failed to load from " .. file)
                    print("Error: " .. thisbylookup)
                end
            end
        end
    end
    manipulatorregistry._lookup = masterlookup
    manipulatorregistry._data = masterkey
end

function manipulatorregistry.dump_lookup_table()
    local pretty = require "pl.pretty"
    pretty.dump(manipulatorregistry._lookup)
end

function manipulatorregistry.find_manipulator_for_appliance(appliance_id)
    local matching_manips = {}

    for regex,manipid in pairs(manipulatorregistry._lookup) do
        if (string.find(appliance_id, regex) ~= nil) then
            local matching_manip = manipulatorregistry._data 
            matching_manips[#matching_manips + 1] = manipulatorregistry._data[manipid]
        end
    end
    return matching_manips
end

function manipulatorregistry.get_download_url_for_manipulator(manipulator_id)
    local maniptable = manipulatorregistry._data[manipulator_id]
    if (maniptable == nil) then
        return nil
    end
end

function manipulatorregistry.download_manipulator_script(manipulatordef, save_to)
    if (manipulatordef["url"] == nil) then error("ERROR: No url defined for manipulator") end
    if (manipulatordef["shasum"] == nil) then error("ERROR: No sha[256]sum defined for manipulator") end
    local has_downloaded = manipulatorregistry.download_registry(save_to, manipulatordef["url"])
    if (has_downloaded ~= true) then
        return false
    end
    return true
end

--- returns id: {"name": name, "compatible": boolean, "fixup": boolean}

function manipulatorregistry.get_compatible_appliances(hardwareIDs)
    local appliances = {}

    for appidx in pairs(storelist) do
        local appinfo = storelist[appidx]
        local compatible= appstore.checkCompatibility(appinfo, ourhardwareIDs)
        local appid = appinfo["id"]
        local manipulators = manipulatorregistry.find_manipulator_for_appliance(appid)
        local appname = appinfo["name"]
        local applianceinfo = {}
        applianceinfo["name"] = appname
        applianceinfo["compatible"] = compatible
        applianceinfo["fixup"] = (#manipulators > 0)
        appliances[appid] = appid
    end
    return appliancess
end

return manipulatorregistry

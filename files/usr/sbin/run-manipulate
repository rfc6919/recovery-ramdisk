#!/usr/bin/env lua

local app = require "pl.app"
local pldir = require "pl.dir"
local utils = require "pl.utils"
local plfile = require "pl.file"
local pretty = require "pl.pretty"

local manipulator = require "manipulator"

local flags, params = app.parse_args(nil,
    { "userpw", "netintf", "hostname", "locale"},  -- list of flags taking values
     {"help","force-pw-change","dhcpv6"})      -- list of allowed flags (value ones will be added)

if (type(flags) ~= "table") then
    print("ERROR parsing command line arguments: ")
    print(flags)
end

local num_params = #params
if (num_params < 2 or flags["help"] ~= nil) then
    print("ERROR: Parameters required: <manipulatorscript.lua> <device>")
    print("\nFor example: run-manipulate ubuntu-manipulator.lua /dev/nvme0n1")
    os.exit(1)
end

local script = params[1]
local target_device = params[2]

-- Load script
print("Using manipulator script: " .. script)
local scriptfile = plfile.read(script)

local password = flags["userpw"]
local hostname = flags["hostname"]

local cconfigDict = manipulator.create_cloud_config(password, hostname)

if (flags["netintf"] ~= nil) then
    cconfigDict["_netintf"] = flags["netintf"]
    if (flags["dhcpv6"] == true) then
        cconfigDict["_dhcpv6"] = true
    end
end

if (flags["force-pw-change"] ~= true) then
    local chpasswdDict = {}
    chpasswdDict["expire"] = false
    cconfigDict["chpasswd"] = chpasswdDict
end

if (flags["locale"] ~= nil) then
    cconfigDict["locale"] = flags["locale"]
end

local scriptenv = utils.load(scriptfile, script, "t")
scriptenv()
do_manipulate(target_device, "/mnt/deployroot/", cconfigDict)

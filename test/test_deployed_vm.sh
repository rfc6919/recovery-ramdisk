#!/bin/sh
set -e

RECOVERY_KERNEL=${RECOVERY_KERNEL:-"output/Image"}
RECOVERY_ROOTFS_COMPRESSED=${RECOVERY_ROOTFS_COMPRESSED:-"output/recovery-root.img.gz"}

[ ! -f "${RECOVERY_KERNEL}" ] && echo "ERROR: No kernel file present at ${RECOVERY_KERNEL}" && exit 1
[ ! -f "${RECOVERY_ROOTFS}" ] && [ ! -f "${RECOVERY_ROOTFS_COMPRESSED}" ] && echo "ERROR: No rootfs file present at ${RECOVERY_ROOTFS}" && exit 1

if [ -f "${RECOVERY_ROOTFS}" ]; then
	gzip -c "${RECOVERY_ROOTFS}" > "${RECOVERY_ROOTFS_COMPRESSED}"
fi

KVM_OPTIONS=""
CPU_OPTIONS="cortex-a53"
if [ $(uname -m) = "aarch64" ]; then
	echo "Native aarch64 host, using KVM"
	KVM_OPTIONS="--enable-kvm"
	CPU_OPTIONS="host"
else
	echo "WARNING: It is strongly recommended to use a native aarch64 host as"
	echo "some operations do not work well under emulation"
fi

QEMU_EFI_BIN=${QEMU_EFI_BIN:-/usr/share/qemu-efi-aarch64/QEMU_EFI.fd}

QEMU_COMMAND_LINE="qemu-system-aarch64 ${KVM_OPTIONS} -m 2084 -cpu ${CPU_OPTIONS} -M virt -nographic"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -bios ${QEMU_EFI_BIN}"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -netdev tap,id=recoverylan,script=no -net nic,netdev=recoverylan"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -netdev user,id=recoverywan -net nic,netdev=recoverywan"
QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -drive file=test.qcow2,if=none,id=drivetest -device virtio-blk,drive=drivetest,serial=testdrive123"
if [ -d "${HUGETLB_MOUNT}" ]; then
        QEMU_COMMAND_LINE="${QEMU_COMMAND_LINE} -mem-path ${HUGETLB_MOUNT}"
fi

echo "${QEMU_COMMAND_LINE}" | xargs -o sudo
